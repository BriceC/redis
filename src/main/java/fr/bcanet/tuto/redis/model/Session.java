package fr.bcanet.tuto.redis.model;

import fr.bcanet.tuto.redis.repository.RedisRecord;

public class Session extends RedisRecord {
	private String token;
	private String connectionDate;


	@Override
	public String key() {
		return token;
	}

	public String getConnectionDate() {
		return connectionDate;
	}

	public String getToken() {
		return token;
	}

	public void setConnectionDate(String connectionDate) {
		this.connectionDate = connectionDate;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
