package fr.bcanet.tuto.redis.model;

import fr.bcanet.tuto.redis.repository.RedisRecord;

public class Person extends RedisRecord {
	private String firstName;
	private String lastName;

	@Override
	public String key() {
		return getLastName();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
