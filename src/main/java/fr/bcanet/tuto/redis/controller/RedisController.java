package fr.bcanet.tuto.redis.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.bcanet.tuto.redis.model.Person;
import fr.bcanet.tuto.redis.repository.RedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
public class RedisController {
    @Autowired
    RedisRepository redisRepository;

    @PutMapping("/addPerson")
    public ResponseEntity<Void> addPerson(@RequestBody Person person) {
        try {
            redisRepository.addRecord(person);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (final JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/persons")
    public ResponseEntity<Map<String, Person>> listPerson() {
        try {
            Map<String, Person> persons = redisRepository.getAllRecords(Person.class);
            return ResponseEntity.ok(persons);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/person/{lastName}")
    public ResponseEntity<Person> getPerson(@PathVariable("lastName") String lastName) {
        try {
            Person p = redisRepository.getRecord(Person.class, lastName);
            return ResponseEntity.ok(p);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping("/person/{lastName}")
    public ResponseEntity<Void> deletePerson(@PathVariable("lastName") String lastName) {
        redisRepository.deleteRecord(Person.class, lastName);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
