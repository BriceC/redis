package fr.bcanet.tuto.redis.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.bcanet.tuto.redis.model.Person;
import fr.bcanet.tuto.redis.model.Session;
import fr.bcanet.tuto.redis.repository.RedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
public class SessionController {
    @Autowired
    RedisRepository redisRepository;

    @PutMapping("/addSession")
    public ResponseEntity<Void> add(@RequestBody Session session) {
        try {
            redisRepository.addRecord(session);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (final JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/sessions")
    public ResponseEntity<Map<String, Session>> listSession() {
        try {
            Map<String, Session> sessions = redisRepository.getAllRecords(Session.class);
            return ResponseEntity.ok(sessions);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/session/{token}")
    public ResponseEntity<Session> getSession(@PathVariable("token") String token) {
        try {
            Session s = redisRepository.getRecord(Session.class, token);
            return ResponseEntity.ok(s);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping("/session/{token}")
    public ResponseEntity<Void> deletePerson(@PathVariable("lastName") String lastName) {
        redisRepository.deleteRecord(Person.class, lastName);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
