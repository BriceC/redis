package fr.bcanet.tuto.redis.repository;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class RedisRepository {
    @Autowired
    RedisOperations<String, Object> redis;

    public void addRecord(RedisRecord redisRecord) throws JsonProcessingException {
        redis.opsForHash().put(redisRecord.getClass().getSimpleName(), redisRecord.key(), redisRecord.writeValueAsString());
    }

    public <T extends RedisRecord> T getRecord(Class<T> type, String id) throws IOException {
        final Object record = redis.opsForHash().get(type.getSimpleName(), id);
        if (record != null) {
            return new ObjectMapper().readValue(record.toString(), type);
        }
        return null;
    }

    public void deleteRecord(Class<? extends RedisRecord> type, String id) {
        redis.opsForHash().delete(type.getSimpleName(), id);
    }

    public <T extends RedisRecord> Map<String, T> getAllRecords(Class<T> type) throws IOException {
        Map<String, T> records = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        Map<Object, Object> entryAsString = redis.opsForHash().entries(type.getSimpleName());

        for (Map.Entry<Object, Object> record:entryAsString.entrySet()) {
            records.put(record.getKey().toString(), mapper.readValue(record.getValue().toString(), type));
        }

        return records;
    }
}
