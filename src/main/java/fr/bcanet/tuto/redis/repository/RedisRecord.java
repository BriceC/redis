package fr.bcanet.tuto.redis.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class RedisRecord {

	public abstract String key();

	public String writeValueAsString() throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(this);
	}
}
